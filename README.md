# C_Newport_XPS-RL

C library for Newport XPS-RL motor controller. Some of it is generic, but some of it is specific to URS100BPP rotation table and BGS80PP goniometer

More info here: https://www.gdargaud.net/Hack/Newport.html
