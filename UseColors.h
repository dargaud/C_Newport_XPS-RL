#ifndef __USE_COLORS_H
#define __USE_COLORS_H


// #define USE_COLOR_MSG
// or use -DUSE_COLOR_MSG in the Makefile

// If defined, all you need to output text in color on the console
// is to start your printf with one of those color macros and end it with NRM (for NoRMal)
// printf(RED "some red text and %d\n" NRM, I);

// 2 command line tests:
// for i in {1..9}; do echo -e "\e[$i""m different style\e[0m"; done
// for i in {30..37} {90..97}; do echo -e "\e[$i""m colorful text\e[0m"; done

#ifdef USE_COLOR_MSG
	#define NRM "\e[0m"		// Normal

// Foreground colors
	#define DFC "\e[39m"	// Default foreground color

	#define BLK "\e[30m"	// Black
	#define DRD "\e[31m"	// Red
	#define GGR "\e[32m"	// Green
	#define DYE "\e[33m"	// Yellow
	#define DBL "\e[34m"	// Blue
	#define DPR "\e[35m"	// Purple
	#define DCY "\e[36m"	// Cyan
	#define GRE "\e[37m"	// Light grey

	#define DRK "\e[90m"	// Dark grey
	#define RED "\e[91m"	// Light red
	#define GRN "\e[92m"	// Light green
	#define YEL "\e[93m"	// Light yellow
	#define BLU "\e[94m"	// Light blue
	#define PRP "\e[95m"	// Light magenta
	#define CYA "\e[96m"	// Light Cyan

	#define WHT "\e[97m"	// White


// Styles:
	#define BLD "\e[1m"		// Bold
	#define DIM "\e[2m"		// Dim
	#define ITL "\e[3m"		// Italic
	#define UND "\e[4m"		// Underline
	#define BLN "\e[5m"		// Blink
	#define REV "\e[7m"		// Reverse
//	#define HID "\e[8m"		// Hidden
	#define STK "\e[9m"		// Strike
	

// Background colors
	#define BDBC "\e[49m"	// Default background color

	#define BBLK "\e[40m"	// Black
	#define BDRD "\e[41m"	// Red
	#define BGGR "\e[42m"	// Green
	#define BDYE "\e[43m"	// Yellow
	#define BDBL "\e[44m"	// Blue
	#define BDPR "\e[45m"	// Purple
	#define BDCY "\e[46m"	// Cyan
	#define BGRE "\e[47m"	// Light grey

	#define BDRK "\e[100m"	// Dark grey
	#define BRED "\e[101m"	// Light red
	#define BGRN "\e[102m"	// Light green
	#define BYEL "\e[103m"	// Light yellow
	#define BBLU "\e[104m"	// Light blue
	#define BPRP "\e[105m"	// Light magenta
	#define BCYA "\e[106m"	// Light Cyan

	#define BWHT "\e[107m"	// White

#else
	#define NRM

	#define DFC
	#define BLK
	#define DRD
	#define GGR
	#define DYE
	#define DBL
	#define DPR
	#define DCY
	#define GRE
	#define DRK
	#define RED
	#define GRN
	#define YEL
	#define BLU
	#define PRP
	#define CYA
	#define WHT

	#define BLD
	#define DIM
	#define ITL
	#define UND
	#define BLN
	#define REV
//	#define HID
	#define STK
	
	#define BDBC
	#define BBLK
	#define BDRD
	#define BGGR
	#define BDYE
	#define BDBL
	#define BDPR
	#define BDCY
	#define BGRE
	#define BDRK
	#define BRED
	#define BGRN
	#define BYEL
	#define BBLU
	#define BPRP
	#define BCYA
	#define BWHT
#endif

#endif
