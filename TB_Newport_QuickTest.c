#include <stdlib.h>
#include <stdio.h>
#include <iso646.h>
#include <string.h>

#include "TB_Newport.h"
#include "UseColors.h"

#define SYNTAX printf(BLD WHT "%s [U=pos] [B=pos] [UB=posU,posB] [...]\n"\
						"\tU=pos\t\tMove URS100 rotation table to position pos (in deg)\n"\
						"\tB=pos\t\tMove BGS80 goniometer to position pos (in deg)\n" \
						"\tUB=posU,posB\tSimultaneously move U and B axes to respective positions posU and posB (in deg)\n" \
						NRM, argv[0]);

int main(int argc, const char* argv[]) {
	int Ret, i;
	double LastU=0, LastB=0;
	if (argc==1 or
		argc>1 and 0==strcmp(argv[1], "-h")) { SYNTAX; return 1; }
	
	if (( Ret=TB_Newport_Init() )) return Ret;

	for (i=1; i<argc; i++)
		if (0==strncmp(argv[i], "U=", 2)) {
			if (( Ret=TB_Newport_MoveAbsUB(LastU=atof(argv[i]+2), LastB) )) break;
		} else if (0==strncmp(argv[i], "B=", 2)){
			if (( Ret=TB_Newport_MoveAbsUB(LastU, LastB=atof(argv[i]+2)) )) break;
		} else if (0==strncmp(argv[i], "UB=", 3)) {
			double U=0, B=0;
			int N=sscanf(argv[i], "UB=%lf,%lf", &U, &B);
			if (N!=2) { SYNTAX; continue; }
			if (( Ret=TB_Newport_MoveAbsUB(LastU=U, LastB=B) )) break;
		} else { printf(BLD RED "Syntax error, unknown parameter '%s'" NRM, argv[i]); SYNTAX; }

	if (Ret) printf(BLD RED "Sequence aborted at step %d\n" NRM, i);
	TB_Newport_Close();
	return Ret;
}
