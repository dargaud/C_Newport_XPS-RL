#ifndef __TB_NEWPORT_H
#define __TB_NEWPORT_H

// User settable parameters - Careful with those as they may cause hardware breakage
typedef struct sNP {
	double MaxU;		// Software limits for rotation table wrap around. From MAX_U-360 to MAX_U
						// In other words, at 0 angle, the cables of the URS and BGS are aligned, facilitating mechanical assembly

	int WrapAround;		// 1: When using MAX_U software limits, if requested position go beyond, will move 360⁰ around to avoid cord wrap
						// 0: return position error if requested position go beyond limits

	double Dp;			// Distance in mm between rotation axis of BGS80PP (B) and sensor plane
						// This needs to be measured precisely.
						// The distance between the base of the craddle and the rotation axis is precisely 100mm

	char IP[80];		// Should be set before calling TB_Newport_Init()
	int Port;

	int msWait;			// Min delay between command and reply and new command.
						// This probably doesn't matter on the Newport as the commands seem to be blocking on the socket until a reply arrives
	int msMotionPolling;// Same as above, but higher value to limit the number of polling messages
} tNP;
extern tNP NP;

///////////////////////////////////////////////////////////////////////////////
extern int TB_Newport_Init(void);
extern int TB_Newport_Close(void);

extern int TB_Newport_MoveAbsUB(double U, double B);

extern int TB_Newport_GetPosUB(double *U, double *B);

extern int TB_Newport_SetSpeedAccelU(double Speed, double Accel);
extern int TB_Newport_SetSpeedAccelB(double Speed, double Accel);

extern char* TB_Newport_Status(void);
extern char* TB_Newport_LastErrMsg(void);

#endif
