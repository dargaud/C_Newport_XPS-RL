newport = TB_Newport

REMOTE?=0	# This is when running test progs from remote system via ssh tunnels. Also use "make REMOTE=1"
COPT = -ggdb -O0 -Wall -std=gnu11 -fdiagnostics-color=always -DUSE_COLOR_MSG -DREMOTE=${REMOTE}
CC=gcc
TP=-DSTANDALONE_TEST_PROG

$(newport): $(newport).c $(newport)_QuickTest.c
	@# $(CC) $(COPT) -Wno-unused-function -c $<		# Compile as obj to test lib capabilities (optional)
	$(CC) $(COPT)                         $< -o $@ $(TP)	# Compile as standalone test program
	$(CC) $(COPT) -Wno-unused-function    $^ -o $@_QuickTest

clean:
	rm -f $(newport) *_QuickTest core* *.o
