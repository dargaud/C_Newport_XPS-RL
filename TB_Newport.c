// NOTE: The communication protocol has ben reversed engineered from the windows-only file XPS_Q8_drivers.cpp

// NOTE: Connect the HOST ethernet of the newport to the ethernet switch inside the Kolmorgen box

// NOTE: HOST ethernet IP: 192.168.0.20 (this has been set through the REMOTE ethernet plug which has an IP of 192.168.254.254 and needs to be done only once)

// NOTE: a default or quick configuration creates 2 groups, one for the URS, one for the BGS. You cannot do simultaneous motions with this configuration (even with non-blocking sockets). You have to define one XY group (named UB), with two positionners: PosU for the URS, PosB for the BGS. The names in the config must match the names in the code here.

// NOTE: U=0 B=45 points towards -Y, it rotates in trigonometric direction so U=90 B=45 points towards +X 
//       (remember that X and Y are reversed in respect to habitual XY axes)

// NOTE: U=-90 aligns the cable of the URS with the cable of the BGS, so for mechanical reasons we want to move +/-180deg from there,
//       so from -270 to +90. See

/*
1	URS100 Precision Rotation Stage, Stepper, Continuous 360deg Motion
2	BGS80 Goniometer, +/-45 deg Travel Range, Micro-step drive

Default configuration (for single axes) - ******** NOT TO BE USED, USE XY COMMON GROUP INSTEAD ********
Slot	Stage model	Driver model	Configuration in StageDataBase	Name
1	URS100BPP	XPS-DRV01	URS@URS100BPP@XPS-DRV01	Group1.Pos
2	BGS80PP	XPS-DRV01	BGS@BGS80PP@XPS-DRV01	Group2.Pos

Front Panel -> Move
Initialize
Home
View/Set

The default configuration is 2 groups, each one with a motor.
We later changed that to one group with 2 motors. It allows for simultaneous motion

1------------------------
	Group1.Pos (rotating)
	Velocity:		40
	Acceleration:		160
	Minimum jerk time:		0.005
	Maximum jerk time:		0.05
	Absolute motion: -165 to 165 ??? where did that come from ? Probably some default. Real range is +/-180
	
HomeSearchSequenceType = MechanicalZeroHomeSearch
HomeSearchMaximumVelocity = 20 ; Unit/Sec
HomeSearchMaximumAcceleration = 80 ; Unit/Sec^2
HomeSearchTimeOut = 34 ; Sec
HomingSensorOffset = 0 ; Unit

2-----------------------
	Group2.Pos (angular)
	Velocity:		20
	Acceleration:		80
	Minimum jerk time:		0.005
	Maximum jerk time:		0.05
	Absolute motion: -45 to 45
	
HomeSearchSequenceType = MechanicalZeroHomeSearch
HomeSearchMaximumVelocity = 10 ; Unit/Sec
HomeSearchMaximumAcceleration = 40 ; Unit/Sec^2
HomeSearchTimeOut = 18 ; Sec
HomingSensorOffset = 0 ; Unit

System / Manual Configuration / Group Families / Single axis groups (2 defined): Group1 (URS) slot 1, Group2 (BGS) slot 2
Stages: BGS and URS
*/

#include <iso646.h>
#include <stdlib.h>		// exit
#include <stdio.h>		// printf
#include <string.h>		// strlen
#include <stdarg.h>
#include <errno.h>
#include <math.h>
#include <ctype.h>		// iscntrl
#include <unistd.h>		// close
#include <signal.h>		// Ctrl-C trap
#include <sys/socket.h>	//socket
#include <arpa/inet.h>	//inet_addr
//#include <netinet/in.h>

#include "UseColors.h"
#include "TB_Newport.h"

tNP NP={
	.IP="192.168.0.20",
	.Port=5001,
	// Globals
	.Dp=100.,
	.MaxU=180.,
	.WrapAround=1,
	.msWait=10,
	.msMotionPolling=500
};
#define msleep(ms) usleep((ms)*1000)	// ms sleep
#define MotionDelay 1		// arbitrary 1s between each position from argv

// So that the various instruments have different alignments to make their logging more readable when intermixed
static int Tidx=0;
static char *Tabs[]={"\t\t\t\t", "\t\t\t\t\t", "\t\t\t\t\t\t"};
#define MAX_REPLY (1024*1024)	// Some replies can be quite big

static int sock=0;	// Socket
				
static int CloseAll(void);
static int SendCommand(char* Reply, const char* Command, ... )
						__attribute__ (( __format__( __printf__, 2, 3 ) ));
static int Enabled=0;

enum { D_ALL, D_NO_REPLY, D_NONE };	// Display cmd&reply, just cmd or neither
static int Display=D_ALL;	// Global to skip displaying some commands or replies
static int BigReply=0;		// Global bool to wait longer when expecting large replies (otherwise internet port is too slow and we don't wait enough)

static char LastErrMsg[1024]="";


typedef struct sPos {		// Positionner structure
	char PN[64];			// Positionner name
	char StageName[64], SmartStageName[64];
	double MinimumTargetPosition, MaximumTargetPosition, 
		MaximumVelocity, MaximumAcceleration, HomeSearchTimeOut,
		Velocity, Acceleration, MinJerkTime, MaxJerkTime,
		MinSoft, MaxSoft,
		LastPos;			// Last set or read position
} tPos;

typedef struct sGrp {		// One group can contain 3 positionners
	char GN[64];			// Group name
	tPos Pos[4];			// 1-based, and 3 is not used, so 1 or 2
	int Status;
} tGrp;
static tGrp Group;

static int AxisOfU=-1, AxisOfB=-1;	// Will be auto-detected. Should be 1 or 2

static void (*PreviousSignalHandlerPipe)(int) = NULL;
static void (*PreviousSignalHandlerInt )(int) = NULL;

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Trap handler to disable drives and close sockets properly
////////////////////////////////////////////////////////////////////////////////
static void intHandler(int Signal) {
	if (Signal==SIGPIPE) puts(BLD RED "\n*** Broken pipe - Closing Newport ***\n" NRM);
	if (Signal==SIGINT ) puts(BLD RED "\n*** Abort - Closing Newport ***\n" NRM);
	// CloseAll();
	TB_Newport_Close();
	if (Signal==SIGPIPE and PreviousSignalHandlerPipe) PreviousSignalHandlerPipe(Signal);
	if (Signal==SIGINT  and PreviousSignalHandlerInt)  PreviousSignalHandlerInt (Signal);
	exit(2);
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Initializes socket communication
////////////////////////////////////////////////////////////////////////////////
static int Init(void) {
	struct sockaddr_in server;
	
	// Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		sprintf(LastErrMsg, "Could not create socket. Error: %s", strerror(errno));
		printf(BLD RED "%s\n" NRM, LastErrMsg);
		return errno;
	}
	printf("%sSocket created for Newport\n", Tabs[0]);

	// Prepare the sockaddr_in structure
	server.sin_addr.s_addr = inet_addr(NP.IP);
	server.sin_family = AF_INET;
	server.sin_port = htons( NP.Port );

	// Connect to remote server
	if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) 	{
		sprintf(LastErrMsg, "Connect to %s:%d failed. Error: %s", NP.IP, NP.Port, strerror(errno));
		printf(BLD RED "%s\n" NRM, LastErrMsg);
		return errno;
	}
	
	PreviousSignalHandlerPipe=signal(SIGPIPE, intHandler);	// Note: when used as a lib we can't have multiple of those. See https://stackoverflow.com/questions/17102919/is-it-valid-to-have-multiple-signal-handlers-for-same-signal
	PreviousSignalHandlerInt =signal(SIGINT,  intHandler);   // Ctrl-C
	//	printf("Previous handlers: pipe=%p, int=%p\n", PreviousSignalHandlerPipe, PreviousSignalHandlerInt);
	
	printf(BLD YEL "%sNewport connected\n" NRM, Tabs[0]);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Close all socket communications
////////////////////////////////////////////////////////////////////////////////
static int CloseAll(void) {
//	char Reply[MAX_REPLY];
	if (sock) {
//		if (Enabled) SendCommand(i, Reply, "DRV.DIS");
		close(sock); sock=0;
	}
	fflush(NULL);
	printf("%sBye\n", Tabs[0]);
    return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN    Send a command to an arm
/// HIPAR	Reply/Contains the useful part of the reply
/// HIRET	Error code. errno is set if socket error, otherwise it is the returned XPS error code (see XPS_Q8_errors.h or GetErrorString())
////////////////////////////////////////////////////////////////////////////////
static int SendCommand(char* Reply, const char* Command, ... ) {
	int R=0, N, L=strlen(Command);
	char Cmd[256];
	va_list str_args;

	va_start( str_args, Command );
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wformat"
	/*int Nb=*/ vsnprintf(Cmd, 256, Command, str_args);
//#pragma clang diagnostic pop
	va_end( str_args );
		
	if (L>2 and (Cmd[L-2]!='\r' or Cmd[L-1]!='\n'))
		strcat(Cmd, "\r\n");
	Reply[0]='\0';
	if (send(sock, Cmd, strlen(Cmd), 0) < 0) {
		printf(BLD RED "Send failed. Error: %s\n" NRM, strerror(errno));
		return errno;
	}
	if (Display!=D_NONE) {
		char Short[256];
		strcpy(Short, Cmd);
		Short[strlen(Cmd)-2]='\0'; // Skip \r\n
		printf("Sent :%s%s\n", Tabs[Tidx], Short);
	}
	msleep(BigReply?1500:NP.msWait);

	// Receive a reply from the server - This is blocking
//    if (recv(sock, Reply, MAX_REPLY, 0) < 0) {
	if ((N=read(sock, Reply, MAX_REPLY)) < 0) {
		printf(BLD RED "%sRecv failed. Error: %s\n" NRM, Tabs[Tidx], strerror(R=errno));
		goto Skip;
	}
	Reply[N]='\0';

#if 0
	if (Display==D_ALL)
		printf("Reply:%s%s***\n", Tabs[Tidx], Reply);
#else
	if ((L=strlen(Reply))>=11) {		// Includes empty replies
		// Reply is normally of the form: 0,msg,EndOfAPI
		// Here we keep only the middle part
		// If this fails, we keep the original string
		R=atoi(Reply);	// Get the error code
		char *Q, *P=strchr(Reply, ',');
		if (P and 0==strcmp(Reply+L-9, ",EndOfAPI")) {
			for (P++, Q=Reply; Q<Reply+L-11; ) 
				if (iscntrl(*P)) P++; else *Q++=*P++;
			*Q='\0';
		}
	}
		
	if (Display==D_ALL)
		printf("Reply:%s%d,%s\n", Tabs[Tidx], R, Reply);
#endif

	msleep(NP.msWait);
Skip:
	return R;
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return error string associated with error code, with bufferization
/// HIPAR	Err/normally 0 to -212 (excluding Zygo). See section "7.9 Error List"
/// HIRET	Error message
////////////////////////////////////////////////////////////////////////////////
static char* GetErrorMsg(int Err) {
	static char* List[256]={NULL};
	char Reply[256];
	Err=abs(Err);
	if (Err>=256) return "Error code out of range";
	if (List[Err]) return List[Err];
	int R=SendCommand(Reply, "ErrorStringGet (%d,char *)", -Err);
	if (R) return "ErrorStringGet failed";
	List[Err]=malloc(strlen(Reply+1));
	if (List[Err]==NULL) return "Out of memory";
	return strcpy(List[Err], Reply);
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return group status string, with bufferization
/// HIPAR	Status/normally 0 to -106. See section "7.8 Group Status List"
/// HIRET	Message
////////////////////////////////////////////////////////////////////////////////
static char* GetGroupStatusMsg(int Status) {
	static char* List[256]={NULL};
	char Reply[256];
	if (Status>=256) return "Status code out of range";
	if (List[Status]) return List[Status];
	int R=SendCommand(Reply, "GroupStatusStringGet (%d,char *)", Status);
	if (R) return "GroupStatusStringGet failed";
	List[Status]=malloc(strlen(Reply+1));
	if (List[Status]==NULL) return "Out of memory";
	return strcpy(List[Status], Reply);
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return controller status string
/// HIFN	We could use ControllerStatusStringGet, but I prefer to minimize calls to the API
/// HIPAR	Status/normally 0 to 0x400. See section "7.10 Controller Status List"
/// HIRET	Message
////////////////////////////////////////////////////////////////////////////////
static char* GetControllerStatusMsg(int Status) {
	static char List[1024]; List[0]='\0';
	if (Status==0) return "Controller status OK";
	if (Status&    1)                                    strcat(List, "Controller initialization failed");
	if (Status&    2) { if (List[0]) strcat(List, ", "); strcat(List, "Number of currently opened sockets reached maximum allowed number"); }
	if (Status&    4) { if (List[0]) strcat(List, ", "); strcat(List, "Controller CPU is overloaded"); }
	if (Status&    8) { if (List[0]) strcat(List, ", "); strcat(List, "Current measured corrector calculation time exceeds the corrector period"); }
	if (Status& 0x10) { if (List[0]) strcat(List, ", "); strcat(List, "Profile generator calculating time exceeds ProfileGeneratorISRRatio * IRSCorrectorPeriod"); }
	if (Status& 0x20) { if (List[0]) strcat(List, ", "); strcat(List, "Controller has lost a corrector interrupt"); }
	if (Status& 0x40) { if (List[0]) strcat(List, ", "); strcat(List, "Zygo interferometer signal is not present"); }
	if (Status& 0x80) { if (List[0]) strcat(List, ", "); strcat(List, "Zygo interferometer Ethernet initialisation failed"); }
	if (Status&0x100) { if (List[0]) strcat(List, ", "); strcat(List, "Zygo interferometer error detected. Please check ZYGO Error Status"); }
	if (Status&0x200) { if (List[0]) strcat(List, ", "); strcat(List, "Motion velocity is limited"); }
	if (Status&0x400) { if (List[0]) strcat(List, ", "); strcat(List, "Lift pin is UP"); }
	return List[0] ? List : "Unknown controller status value";
}

////////////////////////////////////////////////////////////////////////////////
#define CHK(a) if ((Ret=(a))) { \
					sprintf(LastErrMsg, "%s", errno ? strerror(errno) : GetErrorMsg(Ret)); \
					printf(BLD RED "Err:%s%s\n" NRM, Tabs[Tidx], LastErrMsg); \
				} 

////////////////////////////////////////////////////////////////////////////////
static int GroupStatus(void) {	
	char Reply[MAX_REPLY];
	int Ret, Status;
	CHK(SendCommand(Reply, "GroupStatusGet (UB,int *)"));
	printf(BLD YEL "%s%s\n" NRM, Tabs[0], GetGroupStatusMsg(Status=atoi(Reply)));
	return Status;
} 

////////////////////////////////////////////////////////////////////////////////
static int ControllerStatus(void) {	
	char Reply[MAX_REPLY];
	int Ret, Status;
	CHK(SendCommand(Reply, "ControllerStatusGet (int *)"));
	printf(BLD PRP "%s%s\n" NRM, Tabs[Tidx], GetControllerStatusMsg(Status=atoi(Reply)));
	return Status;
} 

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Test if motors are moving
/// HIRET	Number of moving motors (0, 1 or 2, -1 for error)
// HIFN		NOTE: possibly use GroupMoveEndWait instead, but the logic is different
////////////////////////////////////////////////////////////////////////////////
static int GroupMotionStatus(void) {
	char Reply[MAX_REPLY];
	int Ret=0, Pos1=0, Pos2=0; 
	CHK(SendCommand(Reply, "GroupMotionStatusGet (UB,int *,int *)"));
	if (Ret) return -1;
	sscanf(Reply, "%d,%d", &Pos1, &Pos2);
	return Pos1+Pos2;
}

////////////////////////////////////////////////////////////////////////////////
static int InitGroup(void) {
	char Reply[MAX_REPLY];
	int Ret=0; 
	strcpy (Group.GN, "UB");
	CHK(SendCommand(Reply, "GroupKill (UB)"));       if (Ret) return Ret;
	CHK(SendCommand(Reply, "GroupInitialize (UB)")); if (Ret) return Ret;
	Group.Status=GroupStatus();

//	CHK(SendCommand(Reply, "GroupMotionEnable (UB)", G));	// Not, if using GroupKill/GroupInitialize, this is not needed, indeed it returns an error
															// I leave it on purpose to check the error handling
															// Otherwise -> State 42 or 13

	CHK(SendCommand(Reply, "GroupHomeSearch (UB)")); if (Ret) return Ret;
	while (GroupMotionStatus()) putchar('.');
	Group.Status=GroupStatus();
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Initialize a group positionner to be ready for motion
/// HIPAR	G/Slot index of the posiionner: 1 for 'U' or 2 for 'B'
////////////////////////////////////////////////////////////////////////////////
static int InitPositionner(int G) {
	char Reply[MAX_REPLY];
	int Ret=0; 
	if (G<1 or 2<G) { strcpy(LastErrMsg, "Invalid positionner number"); printf(BLD RED "%s\n" NRM, LastErrMsg); return 1; }
	char P=(G==1 ? 'U' : 'B');
	tPos *Pos=&Group.Pos[G];
	sprintf(Pos->PN, "UB.Pos%c", P);
		// Note the .Pos added on some of those. That's pretty much unpredictable but a rule of thumb is:
	// - if the function name has 'Group' in it, don't add .Pos
	// - if the function name has 'Positionner' in it, add .Pos
	CHK(SendCommand(Reply, "SystemIniParameterGet (%s,%s,char *)", Pos->PN, "StageName"));	// URS@URS100BPP@XPS-DRV01 or BGS@BGS80PP@XPS-DRV01
	if (Ret) return Ret;
	strcpy(Pos->StageName, Reply);
	// Also SystemRefParameterGet, FirmwareRefParameterGet

	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,char *)", Pos->PN, "SmartStageName"));	// URS100BPP or BGS80PP
	if (Ret) return Ret;
	strcpy(Pos->SmartStageName, Reply);

	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,char *)", Pos->PN, "MinimumTargetPosition")); Pos->MinimumTargetPosition=atof(Reply);
	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,char *)", Pos->PN, "MaximumTargetPosition")); Pos->MaximumTargetPosition=atof(Reply);
	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,char *)", Pos->PN, "MaximumVelocity"));       Pos->MaximumVelocity=atof(Reply);
	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,char *)", Pos->PN, "MaximumAcceleration"));   Pos->MaximumAcceleration=atof(Reply);
	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,char *)", Pos->PN, "HomeSearchTimeOut"));     Pos->HomeSearchTimeOut=atof(Reply);

	// Same values as above
//	CHK(SendCommand(Reply, "PositionerMaximumVelocityAndAccelerationGet (UB.Pos%c,double *,double *)", P));

	CHK(SendCommand(Reply, "PositionerSGammaParametersGet (%s,double *,double *,double *,double *)", Pos->PN));
	sscanf(Reply, "%lf,%lf,%lf,%lf", &Pos->Velocity, &Pos->Acceleration, &Pos->MinJerkTime, &Pos->MaxJerkTime);
	
	// ??? Values seem nonsensical. Maybe this works only _during_ a motion. Or it needs a special setting beforehand
//	CHK(SendCommand(Reply, "GroupVelocityCurrentGet (UB,double *)"));
//	CHK(SendCommand(Reply, "GroupVelocitySetpointGet (UB,double *)"));
//	CHK(SendCommand(Reply, "GroupAccelerationCurrentGet (UB,double *)"));
//	CHK(SendCommand(Reply, "GroupAccelerationSetpointGet (UB,double *)"));
	// Possibly see PositionerAccelerationAutoScaling

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Stop the motors
////////////////////////////////////////////////////////////////////////////////
static void CloseGroup(void) {
	char Reply[MAX_REPLY];
	int Ret=0; 
//	CHK(SendCommand(Reply, "KillAll ()"));

	CHK(SendCommand(Reply, "GroupMotionDisable (UB)"));	// -> State 20
	sleep(1);
	Group.Status=GroupStatus();
}

// We could also use bind() and check on EADDRINUSE, but it's more complicated

////////////////////////////////////////////////////////////////////////////////
// This is with 2 axes set in XY mode
#ifdef STANDALONE_TEST_PROG
int main(int argc, const char* argv[]) {
	char Reply[MAX_REPLY];
	int Ret=0;
	errno=0;

	if (argc>1 and (0==strcmp("-h", argv[1]) or 0==strcmp("--help", argv[1]))) {
		fprintf(stderr, "%s [PosU PosB] [PosU PosB] ...\n"
				"\tPosU:\tAbsolute angular position to move the rotational table to (in degrees)\n"
				"\tPosB:\tAbsolute angular position to move the goniometer to (in degrees)\n"
				, argv[0]);
		return 1;
	}

	if (Init()) return errno;

	CHK(SendCommand(Reply, "FirmwareVersionGet (char *)"));
	CHK(SendCommand(Reply, "FirmwareBuildVersionNumberGet (char *)"));
	CHK(SendCommand(Reply, "InstallerVersionGet (char *)"));
//	SendCommand(Reply, "GetVerCommand (char *)");	// not interesting
	CHK(SendCommand(Reply, "ObjectsListGet (char *)"));
	ControllerStatus();
	
//	SendCommand(Reply, "ErrorStringGet (%d,char *)", -111 /*ErrorCode*/); 	// Just for a test

//	CHK(SendCommand(Reply, "InitializeAndHomeXY (%s)", "Option0"));	

	if (InitGroup()) goto Skip;
	if (InitPositionner(Tidx=1)) goto Skip;
	if (InitPositionner(Tidx=2)) goto Skip;
	Tidx=0;

	// Verify that the proper motor is on the proper terminal
	for (int i=0; i<4; i++) {
		if (0==strcmp("URS100BPP", Group.Pos[i].SmartStageName)) 
			printf(BLD GRN "%sFound stage name %s on positionner %s\n" NRM, Tabs[0], "URS100BPP", Group.Pos[AxisOfU=i].PN);
		if (0==strcmp("BGS80PP",   Group.Pos[i].SmartStageName)) 
			printf(BLD GRN "%sFound stage name %s on positionner %s\n" NRM, Tabs[0], "BGS80PP", Group.Pos[AxisOfB=i].PN);
	}
	if (AxisOfU==-1)
		printf(BLD RED "URS100BPP not found - log onto the newport and check your configurations\n" NRM);
	if (AxisOfB==-1)
		printf(BLD RED "BGS80PP not found - log onto the newport and check your configurations\n" NRM);
	if (AxisOfU==-1 or AxisOfU==-1) return 1;

	Group.Pos[AxisOfU].MinSoft=NP.MaxU-360;
	Group.Pos[AxisOfU].MaxSoft=NP.MaxU;
	Group.Pos[AxisOfB].MinSoft=Group.Pos[AxisOfB].MinimumTargetPosition;
	Group.Pos[AxisOfB].MaxSoft=Group.Pos[AxisOfB].MaximumTargetPosition;

	for (int i=0; i<argc/2; i++) {
		printf(BLD WHT "================== Moving %d to: U:%.3f, B:%.3f ==================\n" NRM,
				i, Group.Pos[AxisOfU].LastPos=atof(argv[i*2+1]), Group.Pos[AxisOfB].LastPos=atof(argv[i*2+2]));
		// Sequential motion
		CHK(SendCommand(Reply, "GroupMoveAbsolute (%s,%.3g,%.3g)", "UB", Group.Pos[AxisOfU].LastPos, Group.Pos[AxisOfB].LastPos));
		while (GroupMotionStatus()) putchar('.'), msleep(NP.msMotionPolling);
	
		CHK(SendCommand(Reply, "GroupPositionCurrentGet (%s,double *,double *)", "UB")); 
		sscanf(Reply, "%lf,%lf", &Group.Pos[AxisOfU].LastPos, &Group.Pos[AxisOfB].LastPos);
		printf(BLD YEL "%sConfirmed positions: U=%.1fdeg B=%.1fdeg\n" NRM, Tabs[0], Group.Pos[AxisOfU].LastPos, Group.Pos[AxisOfB].LastPos);
		
		sleep(MotionDelay);
	}

	CloseGroup();
	ControllerStatus();	
	
Skip:
	printf("================== Closing ==================\n");
//	SendAll("DRV.WARNINGS");
	CloseAll();
	if (Ret) printf(BLD RED "[Error condition present]\n" NRM);
	return Ret;
}
#endif // STANDALONE_TEST_PROG



////////////////////////////////////////////////////////////////////////////////
/// HIFN	Prepare the system for motion operation in attached X+X2 mode
////////////////////////////////////////////////////////////////////////////////
int TB_Newport_Init(void) {
	char Reply[MAX_REPLY];
	int Ret;

	if (Init()) return errno;

	CHK(SendCommand(Reply, "FirmwareVersionGet (char *)"));
	CHK(SendCommand(Reply, "FirmwareBuildVersionNumberGet (char *)"));
	CHK(SendCommand(Reply, "InstallerVersionGet (char *)"));
//	SendCommand(Reply, "GetVerCommand (char *)");	// not interesting
	CHK(SendCommand(Reply, "ObjectsListGet (char *)"));
	ControllerStatus();

	if ((Ret=InitGroup()            )) return Ret;
	if ((Ret=InitPositionner(Tidx=1))) return Ret;
	if ((Ret=InitPositionner(Tidx=2))) return Ret;
	Tidx=0;

	// Verify that the proper motor is on the proper terminal
	for (int i=0; i<4; i++) {
		if (0==strcmp("URS100BPP", Group.Pos[i].SmartStageName)) 
			printf(BLD GRN "%sFound stage name %s on %s\n" NRM, Tabs[i], "URS100BPP", Group.Pos[AxisOfU=i].PN);
		if (0==strcmp("BGS80PP",   Group.Pos[i].SmartStageName)) 
			printf(BLD GRN "%sFound stage name %s on %s\n" NRM, Tabs[i], "BGS80PP",   Group.Pos[AxisOfB=i].PN);
	}
	if (AxisOfU==-1) sprintf(LastErrMsg, "URS100BPP not found - log onto the newport and check your configuration");
	if (AxisOfB==-1) sprintf(LastErrMsg,   "BGS80PP not found - log onto the newport and check your configuration");
	if (AxisOfU==-1 or AxisOfU==-1) {
		printf(BLD RED "%s\n" NRM, LastErrMsg);
		return 1;
	}

	Group.Pos[AxisOfU].MinSoft=NP.MaxU-360;
	Group.Pos[AxisOfU].MaxSoft=NP.MaxU;
	Group.Pos[AxisOfB].MinSoft=Group.Pos[AxisOfB].MinimumTargetPosition;
	Group.Pos[AxisOfB].MaxSoft=Group.Pos[AxisOfB].MaximumTargetPosition;

	printf(BLD GRN "================== ROTATION TABLES READY ==================\n" NRM);
	Enabled=1;
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Shut down the system. You will need to call init again
////////////////////////////////////////////////////////////////////////////////
int TB_Newport_Close(void) {
	CloseGroup();
	ControllerStatus();	
	CloseAll(); 
	Enabled=0;
	printf(BLD YEL "================== ROTATION TABLES CLOSED ==================\n" NRM);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Move to absolute position
/// HIPAR	Angle1/in degrees. Range depends on positionner (+/-360 or +/-45)
/// HIPAR	Angle2/in degrees. Range depends on positionner (+/-360 or +/-45)
/// HIRET	0 if no error
////////////////////////////////////////////////////////////////////////////////
static int TB_Newport_MoveAbs(double Angle1, double Angle2) {
	char Reply[MAX_REPLY];
	int Ret;
	double *Angles[3]={NULL,&Angle1, &Angle2};
	if (!Enabled) {
		sprintf(LastErrMsg, "The system is not ready, call TB_Newport_Init() before");
		printf(BLD RED "%s%s\n" NRM, Tabs[0], LastErrMsg);
		return 2;
	}
	
	for (int G=1; G<=2; G++) {
		// Range checking
		if (G==AxisOfU and NP.WrapAround) {	// This is somewhat dangerous and can lead to bent connectors or twisted cables
			while (*Angles[G]<Group.Pos[G].MinSoft) *Angles[G]+=360;
			while (*Angles[G]>Group.Pos[G].MaxSoft) *Angles[G]-=360;
		}
		if (*Angles[G]<Group.Pos[G].MinSoft or Group.Pos[G].MaxSoft<*Angles[G]) {
			sprintf(LastErrMsg, "Requested %c position %.1fdeg out of range [%.0f..%.0f]",
				G==AxisOfU ? 'U' : 'B', *Angles[G], Group.Pos[G].MinSoft, Group.Pos[G].MaxSoft);
			printf(BLD RED "%s%s\n" NRM, Tabs[G], LastErrMsg);
			return G;
		}
		Group.Pos[G].LastPos=*Angles[G];
	}
	
	CHK(SendCommand(Reply, "GroupMoveAbsolute (%s,%.3g,%.3g)", "UB", Group.Pos[1].LastPos, Group.Pos[2].LastPos));

	// I think the above is blocking while the motor is active, so this line never actually waits unless there's a problem
	while (GroupMotionStatus()) putchar('.'), msleep(NP.msMotionPolling);

	// Optional
	CHK(SendCommand(Reply, "GroupPositionCurrentGet (%s,double *,double *)", "UB")); 
	sscanf(Reply, "%lf,%lf", &Group.Pos[1].LastPos, &Group.Pos[2].LastPos);
	printf(BLD YEL "%sConfirmed positions: U=%.1fdeg, B=%.1fdeg\n" NRM, Tabs[0], Group.Pos[AxisOfU].LastPos, Group.Pos[AxisOfB].LastPos);

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
int TB_Newport_MoveAbsUB(double U, double B) {
	return TB_Newport_MoveAbs(AxisOfU==1 ? U : B, AxisOfB==1 ? U : B);
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Get current position
/// OUT		Angle1, Angle2
/// HIPAR	Angle1/in degrees. Pass NULL if you don't need it
/// HIPAR	Angle2/in degrees. Pass NULL if you don't need it
/// HIRET	0 if no error
////////////////////////////////////////////////////////////////////////////////
static int TB_Newport_GetPos(double *Angle1, double *Angle2) {
	char Reply[MAX_REPLY];
	int Ret;
	if (!Enabled) {
		sprintf(LastErrMsg, "The system is not ready, call TB_Newport_Init() before");
		printf(BLD RED "%s%s\n" NRM, Tabs[0], LastErrMsg);
		return 2;
	}
	
	CHK(SendCommand(Reply, "GroupPositionCurrentGet (%s,double *,double *)", "UB")); if (Ret) return Ret;
	sscanf(Reply, "%lf,%lf", &Group.Pos[1].LastPos, &Group.Pos[2].LastPos); 
	if (Angle1!=NULL) *Angle1=Group.Pos[1].LastPos;
	if (Angle2!=NULL) *Angle2=Group.Pos[2].LastPos;
	printf(BLD YEL "%sPosition: U=%.1fdeg B=%.1fdeg\n" NRM, Tabs[0], Group.Pos[AxisOfU].LastPos, Group.Pos[AxisOfB].LastPos);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Get current position
/// OUT		Angle1, Angle2
/// HIPAR	U/in degrees. Pass NULL if you don't need it
/// HIPAR	B/in degrees. Pass NULL if you don't need it
/// HIRET	0 if no error
////////////////////////////////////////////////////////////////////////////////
int TB_Newport_GetPosUB(double *U, double *B) {
	double Angle1, Angle2;
	int Ret=TB_Newport_GetPos(&Angle1, &Angle2); if (Ret) return Ret;
	if (U!=NULL) *U = (AxisOfU==1 ? Angle1 : Angle2);
	if (B!=NULL) *B = (AxisOfB==1 ? Angle1 : Angle2);
	return Ret;
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Set speed and acceleration for both U and B
/// HIPAR	Speed / Speed in deg/s. Use 0 to leave unchanged
/// HIPAR	Accel / Acceleration and deceleration in deg/s^2. Use 0 to leave unchanged
/// RETURN	0 if within bounds
////////////////////////////////////////////////////////////////////////////////
int TB_Newport_SetSpeedAccel(int G, double Speed, double Accel) {
	char Reply[256];
	int Ret;
	if (G<1 or 2<G) { strcpy(LastErrMsg, "Invalid positionner number"); printf(BLD RED "%s\n" NRM, LastErrMsg); return 3; }
	tPos *Pos=&Group.Pos[G];
	// printf(BLD PRP "Speed:%.1f, MaxV:%.1f,  Accel:%.1f, MaxAccel:%.1f\n" NRM, Speed, Pos->MaximumVelocity,  Accel, Pos->MaximumAcceleration);
	if (Speed!=0) {
		if (0<Speed and Speed<=Pos->MaximumVelocity    ) Pos->Velocity    =Speed;
		else {
			sprintf(LastErrMsg, "Speed %.1f not in allowed range 0..%.1f", Speed, Pos->MaximumVelocity);
			printf(BLD RED "%s\n" NRM, LastErrMsg);
			return 31;
		}
	}
	if (Accel!=0) {
		if (0<Accel and Accel<=Pos->MaximumAcceleration) Pos->Acceleration=Accel;
		else {
			sprintf(LastErrMsg, "Acceleration %.1f not in allowed range 0..%.1f", Accel, Pos->MaximumAcceleration);
			printf(BLD RED "%s\n" NRM, LastErrMsg);
			return 32;
		}
	}

	CHK(SendCommand(Reply, "PositionerSGammaParametersSet (%s,%.1f,%.1f,%.3f,%.3f)", 
				Pos->PN, Pos->Velocity, Pos->Acceleration, Pos->MinJerkTime, Pos->MaxJerkTime));
	if (Ret) return Ret;
	
	// Wrong, those are system parameters and should not be changed
//	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,%.0f)", Pos->PN, "MaximumVelocity",     Pos->MaximumVelocity));
//	CHK(SendCommand(Reply, "PositionerStageParameterGet (%s,%s,%.0f)", Pos->PN, "MaximumAcceleration", Pos->MaximumAcceleration));

	return 0;
}

int TB_Newport_SetSpeedAccelU(double Speed, double Accel) { return TB_Newport_SetSpeedAccel(AxisOfU, Speed, Accel); }
int TB_Newport_SetSpeedAccelB(double Speed, double Accel) { return TB_Newport_SetSpeedAccel(AxisOfB, Speed, Accel); }

///////////////////////////////////////////////////////////////////////////////
/// HIFN	General status report of Newport
/// HIRET	Contains name of controller, system status and limits
///////////////////////////////////////////////////////////////////////////////
char* TB_Newport_Status(void) {
	char Reply[256]="";
	int Ret;
	static char String[1024]; String[0]='\0';
	if (!Enabled) return "Newport controller not initialized or closed";

	CHK(SendCommand(Reply, "FirmwareVersionGet (char *)"));
	if (Ret) return LastErrMsg;
	sprintf(String, "%s\n", Reply);

	for (int i=1; i<=2; i++) {
		tPos *Pos=&Group.Pos[i];
		Group.Status=GroupStatus();
		sprintf(String+strlen(String), "%s %s, Hardware limits: %.0f..%.0fdeg, Software limits: %.0f..%.0fdeg%s, "
				"Speed: %.1fdeg/s (max:%.0f), Acceleration: %.1fdeg/s^2 (max:%.0f), "
				"%s\n",
				Pos->PN, Pos->SmartStageName, 
				Pos->MinimumTargetPosition, Pos->MaximumTargetPosition,
				Pos->MinSoft,               Pos->MaxSoft, i==AxisOfU ? " (*)" : "",
				Pos->Velocity,              Pos->MaximumVelocity, 
				Pos->Acceleration,          Pos->MaximumAcceleration,
				GetGroupStatusMsg(Group.Status)
   			);
	}
	sprintf(String+strlen(String), "(*) Wrap around is %s\n", NP.WrapAround
			? "enabled, so the table will do a reverse 360 motion if you exceed those limits to avoid cable wrap"
			: "disabled, so trying to move out of those values will give an error");

	strcat(String, GetControllerStatusMsg(ControllerStatus() /*Group.Status*/ )); 

	return String;
}

///////////////////////////////////////////////////////////////////////////////
char* TB_Newport_LastErrMsg(void) {
	return LastErrMsg;
}
